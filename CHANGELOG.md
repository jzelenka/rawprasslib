# Changelog

### 0.0.12
made conditions for peaks more permisive - allows high intensity spikes without failing

### 0.0.11
updated changelog

### 0.0.10
rewritten a routine for seeking for scan dataset headers (merge from unstable branch)

### 0.0.5-0.0.9
undocumented, part of it were WIP versions of 0.0.10

### 0.0.4.
parameters change during acquisition fixed for LTQ

### 0.0.3.
experimetal support of parameters change during acquisition

### 0.0.2.
added mising variables to setup.py

### 0.0.1.
initial release to pypi.
